package com.sweetrenard.jff.kettle;

import android.app.Application;

public class Kettle extends Application {
	
	private boolean run = false;
	private int cur_set_time = 0;
	private int DEF_TIME = 60 * 1;
	
	public boolean isRun() {
		return run;
	}
	
	public void setState(boolean is_run) {
		this.run = is_run;
	}
	
	public int getCurTime() {
		return cur_set_time;
	}
	
	public void setCurTime(int time) {
		this.cur_set_time = time;
	}
	
	
	public int getDefTime() {
		return DEF_TIME;
	}

}
