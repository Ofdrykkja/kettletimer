package com.sweetrenard.jff.kettle;

import com.sweetrenard.jff.teapot.R;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.Vibrator;
import android.util.Log;

public class TimerService extends Service implements Shaker.Callback {

    private static final String TAG = "#TimerService#";
    private BroadcastReceiver timer_receiver;
    private IntentFilter stop_start_filter = new IntentFilter("com.sweetrenard.jff.teapot.start_stop");
    private Notification.Builder notify_builder = null;
	private CountDownTimer timer;
	private Vibrator vibrator;
	private Shaker shaker = null;
	private Kettle app;
	private static PowerManager.WakeLock sCpuWakeLock;

    @Override
    public void onCreate() {

        Log.d(TAG, "onCreate");
        app = (Kettle)getApplication();
        shaker = new Shaker(this, 1.95d, 500, this);
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        timer_receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
            	toggleTimer();
            }
        };
        initTimer();
        registerReceiver(timer_receiver, stop_start_filter);
        showStatusBarNotification();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        unregisterReceiver(timer_receiver);
        shaker.close();
    }
    
    
    private void showStatusBarNotification() {
    	Intent notificationIntent = new Intent(this, KettleTimer.class);
    	PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
    	notify_builder = new Notification.Builder(this)
    		.setSmallIcon(R.drawable.ic_stat_example)
    		.setTicker(getString(R.string.notification_start_ticket))
    		.setWhen(System.currentTimeMillis())
    		.setContentTitle(getString(R.string.app_name))
    		.setContentText(getString(R.string.notification_start_ticket))
    		.setContentIntent(pendingIntent);
    	startForeground(12345, notify_builder.getNotification());
    }
    
    private Intent update_intent = new Intent("com.sweetrenard.jff.teapot.UPDATE_UI");
    
    private void initTimer() {
    	Log.d(TAG, "current time: " + String.valueOf(app.getCurTime()));
        timer = new CountDownTimer(app.getCurTime() * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            	long sec_until_finished = millisUntilFinished / 1000;
            	update_intent.putExtra("time", (int)sec_until_finished);
            	sendBroadcast(update_intent);
            	app.setCurTime(app.getCurTime() - 1);
            }

            @Override
            public void onFinish() {
                Log.d(TAG, "done!");
                Log.d(TAG, "time at finish: " + app.getCurTime());
                app.setCurTime(0);
                update_intent.putExtra("time", -1);
                app.setState(false);
                sendBroadcast(update_intent);
                showDialogOnLockScreen();
                play();
                //TODO
                //stopForeground(true);
            }
        };
    }
    
    private void toggleTimer() {
    	vibrator.vibrate(300);
    	if (!app.isRun()) {
    		initTimer();
            timer.start();
            app.setState(!app.isRun());
            //TODO
            //showStatusBarNotification();
        } else {
            Log.d(TAG, "timer canceled");
            timer.cancel();
            initTimer();
            app.setState(!app.isRun());
            //stopForeground(true);
        }
    }

	@Override
	public void shakingStarted() {
		// TODO Auto-generated method stub
		toggleTimer();
		sendBroadcast(update_intent);
	}

	@Override
	public void shakingStopped() {
		// TODO Auto-generated method stub
		
	}
	
	private void showDialogOnLockScreen() {
		PowerManager pm =
                (PowerManager) getSystemService(Context.POWER_SERVICE);

        sCpuWakeLock = pm.newWakeLock(
                PowerManager.FULL_WAKE_LOCK |
                PowerManager.ACQUIRE_CAUSES_WAKEUP |
                PowerManager.ON_AFTER_RELEASE, TAG);
        sCpuWakeLock.acquire();
        Intent intent = new Intent(this, KettleTimer.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
	}
	
	private void play() {
		Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
        r.play();
	}

}
