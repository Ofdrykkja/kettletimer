package com.sweetrenard.jff.kettle;

import java.text.DecimalFormat;
import java.util.Arrays;

import android.view.*;
import com.sweetrenard.jff.teapot.R;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.TextView;
import android.widget.ToggleButton;

public class KettleTimer extends Activity implements OnTouchListener {

    private static final String TAG = "#TeapotTimer#";
    private ToggleButton btn_start;
	private TextView time_text;
	private IntentFilter update_ui_filter = new IntentFilter("com.sweetrenard.jff.teapot.UPDATE_UI");
	private BroadcastReceiver update_ui_receiver;
	private Intent start_stop = new Intent("com.sweetrenard.jff.teapot.start_stop");
	private Kettle app;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.main);
        startService(new Intent(this, TimerService.class));
        app = (Kettle)getApplication();
        btn_start = (ToggleButton) findViewById(R.id.btn_start);
        time_text = (TextView) findViewById(R.id.time);
        initReceiver();
        time_text.setOnTouchListener(this);
        btn_start.setChecked(app.isRun());
        btn_start.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	toggleTimer();
            }
        });
        
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }
    
    @Override
    public void onResume() {
    	super.onResume();
    	btn_start.setChecked(app.isRun());
    	if (!app.isRun() && app.getCurTime() == 0) {
   			app.setCurTime(app.getDefTime());
    	}
    	setTimeText(app.getCurTime());
    	registerReceiver(update_ui_receiver, update_ui_filter);
    }
    
    @Override
    public void onPause() {
    	super.onPause();
    	unregisterReceiver(update_ui_receiver);
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.exit:
                stopService(new Intent(getApplicationContext(), TimerService.class));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    private void initReceiver() {
    	update_ui_receiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				// TODO Auto-generated method stub
				int time = intent.getIntExtra("time", 0);
				setTimeText(app.getCurTime());
				btn_start.setChecked(app.isRun());
				Log.d(TAG, String.valueOf(app.getCurTime()));
				if (time == -1) {
					complete();
				}
			}
    		
    	};
    }
    
    private void complete() {
    	time_text.setText("ta-da!");
        //btn_start.setChecked(app.isRun());
    }
    
    private void toggleTimer() {
        sendBroadcast(start_stop);
    }
    
    private static String intToString(int num, int digits) {
    	if (digits > 0) {
	        char[] zeros = new char[digits];
	        Arrays.fill(zeros, '0');
	        DecimalFormat df = new DecimalFormat(String.valueOf(zeros));
	
	        return df.format(num);
    	} else {
    		return String.valueOf(num);
    	}
    }
    
    private void setTimeText(int time_in_seconds) {
    	int sec = time_in_seconds % 60;
    	int min = (int) Math.floor(time_in_seconds / 60);
        time_text.setText(intToString(min, 2) + ":" + intToString(sec, 2));
    }
    
    //touch pos
    private float x;
    private float y;
    private int sx;
    private int sy;
    
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (!app.isRun()) {
		    x = event.getX();
		    y = event.getY();
		    
		    switch (event.getAction()) {
		    case MotionEvent.ACTION_DOWN:
		    	sx = (int)x;
		    	sy = (int)y;
		    	break;
		    case MotionEvent.ACTION_MOVE:
		    	if(sx > x && app.getCurTime() != 0) {
		    		app.setCurTime(app.getCurTime() - 1);
		    	} else if (sx < x) {
		    		app.setCurTime(app.getCurTime() + 1);
		    	}
		    	//TODO add quickly vertical increment
		    	/*else if (sy > y && app.getCurTime() != 0) {
		    		app.setCurTime(app.getCurTime() - 5);
		    	} else if (sy < y) {
		    		app.setCurTime(app.getCurTime() + 5);
		    	}*/
		    	break;
		    case MotionEvent.ACTION_UP:
		    case MotionEvent.ACTION_CANCEL:  
		    	break;
		    }
		    setTimeText(app.getCurTime());
		}
	    return true;
	}
}
